
public class MainSynchro {

    public static void main(String[] args) {
        MyObject o=new MyObject();
        MyThreadS1 t1=new MyThreadS1(o, 5);
        MyThreadS1 t2=new MyThreadS1(o, 20);
        t1.start();
        t2.start();
    }

}
