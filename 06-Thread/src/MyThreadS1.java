
public class MyThreadS1 extends Thread {

    private MyObject obj;
    
    private int v;


    public MyThreadS1(MyObject obj, int v) {
        super();
        this.obj = obj;
        this.v = v;
    }

    @Override
    public void run() {
        obj.print(v);
    }
    
}
