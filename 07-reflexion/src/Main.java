import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

public class Main {

    public static void main(String[] args) {
        // 1
        Class<String> c1=String.class;
        
        // 2
        String str="hello";
        Class<? extends String> c2=str.getClass();
    
        //3
        try {
            Class<?> c3=Class.forName("java.lang.String");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        
        // Nom de la Classe
        System.out.println(c1.getName());
        
        System.out.println(c1.getSuperclass().getName());
        
        // Attributs
        Field[] attr=c1.getDeclaredFields();
        for(Field f : attr) {
            System.out.print(f.getName() +"\t");
            System.out.println(f.getType().getName());
        }
        
        Method[] meth=c1.getDeclaredMethods();
        for(Method m :meth) {
            System.out.print(m.getName()+"\t");
            System.out.print(m.getReturnType().getName()+"\t");
            Parameter[] param = m.getParameters();
            for(Parameter p : param) {
                System.out.println("\t" +p.getName() + " " + p.getType().getName());
            }
        }
        
        Constructor[] construct=c1.getConstructors();
        for(Constructor c: construct ) {
            System.out.println(c);
        }
        
        // Instensiation dynamique
        
        // Avec le constructeur par défaut
        try {
            String strDyn=c1.newInstance();
            System.out.println(strDyn);
            
            Constructor<String> constructeur=c1.getConstructor(new Class[] {String.class});
            String strDyn2=constructeur.newInstance(new Object[] {"test"});
            System.out.println(strDyn2);
            
        } catch (InstantiationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
