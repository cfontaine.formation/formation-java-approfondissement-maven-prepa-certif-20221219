package fr.dawan.formation;

import java.time.LocalDate;
import java.util.List;

import fr.dawan.formation.beans.Employe;
import fr.dawan.formation.dao.EmployeDao;

public class MainDao {

    public static void main(String[] args) {
       EmployeDao dao=new EmployeDao();
       
       Employe emp=new Employe("Lionnel","Durand",LocalDate.of(1987, 5, 12),"ld@dawan.com",1950.0);
       try {
           System.out.println(emp);
           dao.save(emp, false);
           System.out.println(emp);
           
           List<Employe> lst=dao.findAll(false);
           for(Employe em : lst) {
               System.out.println(em);
           }
           
           emp=dao.findById(3, false);
           emp.setSalaire(2100);
           dao.update(emp, false);
           
           dao.remove(4, false);
           
           lst=dao.findAll(false);
           for(Employe em : lst) {
               System.out.println(em);
           }
           
           System.out.println(dao.count(true));
           
           
    } catch (Exception e) {
        e.printStackTrace();
    }

    }

}
