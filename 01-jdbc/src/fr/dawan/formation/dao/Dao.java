package fr.dawan.formation.dao;

import java.util.List;

public interface Dao <T>{

    void save(T entity,boolean close) throws Exception;
    
    void update(T entity,boolean close) throws Exception;
    
    void remove(T entity,boolean close) throws Exception;
    
    void remove(long id,boolean close) throws Exception;
    
    T findById(long id,boolean close) throws Exception;
    
    List<T> findAll(boolean close) throws Exception;
    
    int count(boolean close) throws Exception;
}
