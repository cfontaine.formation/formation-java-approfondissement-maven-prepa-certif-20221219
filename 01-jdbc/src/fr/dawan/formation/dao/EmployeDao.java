package fr.dawan.formation.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.dawan.formation.beans.Employe;

public class EmployeDao implements Dao<Employe> {

    private DbConnection cnx = new DbConnection();

    @Override
    public void save(Employe e, boolean close) throws Exception {
        String req = "INSERT INTO employes(prenom,nom,date_naissance,email,salaire) VALUE(?,?,?,?,?)";
        Connection c = cnx.getConnection();
        PreparedStatement ps = c.prepareStatement(req, Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, e.getPrenom());
        ps.setString(2, e.getNom());
        ps.setDate(3, Date.valueOf(e.getDateNaissance()));
        ps.setString(4, e.getEmail());
        ps.setDouble(5, e.getSalaire());
        ps.executeUpdate();

        ResultSet key = ps.getGeneratedKeys();
        if (key.next()) {
            e.setId(key.getLong(1));
        }
        cnx.closeConnection(close);
    }

    @Override
    public void update(Employe e, boolean close) throws Exception {
        String req="UPDATE employes SET prenom=?, nom=? , date_naissance=?, email=? , salaire=? WHERE id=?";
        Connection c=cnx.getConnection();
        PreparedStatement ps=c.prepareStatement(req);
        ps.setString(1, e.getPrenom());
        ps.setString(2, e.getNom());
        ps.setDate(3, Date.valueOf(e.getDateNaissance()));
        ps.setString(4, e.getEmail());
        ps.setDouble(5, e.getSalaire());
        ps.setLong(6, e.getId());
        ps.executeUpdate();
        cnx.closeConnection(close);
    }

    @Override
    public void remove(Employe e, boolean close) throws Exception {
        remove(e.getId(), close);

    }

    @Override
    public void remove(long id, boolean close) throws Exception {
        Connection c=cnx.getConnection();
        PreparedStatement ps = c.prepareStatement("DELETE FROM employes WHERE id=?");
        ps.setLong(1, id);
        ps.executeUpdate();
        cnx.closeConnection(close);
    }

    @Override
    public Employe findById(long id, boolean close) throws Exception {
        Employe emp = null;
        String req = "SELECT prenom,nom,date_naissance,salaire,email FROM employes WHERE id=?";
        Connection c=cnx.getConnection();
        PreparedStatement ps = c.prepareStatement(req);
        ps.setLong(1, id);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            emp = new Employe(rs.getString("prenom"), rs.getString("nom"), rs.getDate("date_naissance").toLocalDate(),
                    rs.getString("email"), rs.getDouble("salaire"));
            emp.setId(id);
        }
        cnx.closeConnection(close);
        return emp;
    }

    @Override
    public List<Employe> findAll(boolean close) throws Exception {
        List<Employe> lst = new ArrayList<>();
        Connection c = cnx.getConnection();
        String req = "SELECT id,prenom,nom,date_naissance,email,salaire FROM employes";
        PreparedStatement ps = c.prepareStatement(req);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            Employe emp = new Employe(rs.getString("prenom"), rs.getString("nom"),
                    rs.getDate("date_naissance").toLocalDate(), rs.getString("email"), rs.getDouble("salaire"));
            emp.setId(rs.getLong("id"));
            lst.add(emp);
        }
        cnx.closeConnection(close);
        return lst;
    }

    @Override
    public int count(boolean close) throws Exception {
        int count=0;
        Connection c = cnx.getConnection();
        String req="SELECT COUNT(id) FROM employes";
        PreparedStatement ps=c.prepareStatement(req);
        ResultSet rs=ps.executeQuery();
        if(rs.next()) {
            count=rs.getInt(1);
        }
        cnx.closeConnection(close);
        return count;
    }

}
