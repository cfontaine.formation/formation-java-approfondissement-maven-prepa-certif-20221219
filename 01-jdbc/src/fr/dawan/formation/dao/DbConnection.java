package fr.dawan.formation.dao;

import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class DbConnection {

    private Connection cnx;

    private Properties prop;

   
    public Connection getConnection() throws Exception {
        if (cnx == null) {
            if (prop == null) {
                try (FileReader fr = new FileReader("configuration.properties")) {
                    // Les paramètres de connexion de la base de données sont stockés dans le fichiers de propriété : configuration.properties
                    // il doit être placé à la racine du projet
                    prop=new Properties();
                    prop.load(fr);
                }
            }
            // Créer la connexion à la base de donnée
            cnx = DriverManager.getConnection(prop.getProperty("url"), prop.getProperty("user"),prop.getProperty("password"));
        }
        return cnx;
    }
    
    public void closeConnection(boolean close) throws Exception {
        if(close) {
            cnx.close();
            cnx=null;
        }
    }

}
