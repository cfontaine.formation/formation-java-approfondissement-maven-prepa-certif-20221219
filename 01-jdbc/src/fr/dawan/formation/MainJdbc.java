package fr.dawan.formation;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import fr.dawan.formation.beans.Employe;

public class MainJdbc {

    public static void main(String[] args) {
//        TestJdbc();

//        Employe e = new Employe("Jane", "Doe", LocalDate.of(1999, 12, 5), "jane@dawan.com", 2000.0);
//        System.out.println(e);
//        Insert(e);
//        System.out.println(e);

//        e = Select(2);
//        System.out.println(e);
//        delete(e);

        testTransaction(false);
    }

    public static void TestJdbc() {
        Connection cnx = null;
        try {
            // Chargement du pilote de la base de donnée
            Class.forName("com.mysql.cj.jdbc.Driver");

            // Ouverture de la connexion à la base de donnée
            cnx = DriverManager.getConnection("jdbc:mysql://localhost:3306/formation_java", "root", "");

            // Statement => permet d'exécuter une requète SQL
            Statement stm = cnx.createStatement();

            String req = "INSERT INTO employes(prenom,nom,date_naissance,email,salaire) "
                    + "VALUES ('John','Doe','1996/03/09','jd@dawan.fr',1500.0)";

            // Il faut utiliser la méthode executeUpdate pour les requêtes INSERT, DELETE, UPDATE
            stm.executeUpdate(req);

            // Il faut utiliser la méthode executeQuery pour les requêtes SELECT
            // ResultSet => pour récupèrer les résultats de la requête
            req = "SELECT id,prenom,nom,date_naissance,email,salaire FROM employes";
            ResultSet r = stm.executeQuery(req);

            // La méthode next -> passe au résultat suivant (ligne), retourne vrai tant
            // qu'il y a des résultats
            // (au départ, on se trouve juste avant le premier résultat (première ligne))
            while (r.next()) {
                // Les Méthodes getString, getDouble, ... permettent de lire les valeurs des
                // colonnes
                System.out.println(r.getLong("id") + " " + r.getString("prenom") + " " + r.getString("nom") + " "
                        + r.getDate("date_naissance") + " " + r.getString("email") + " " + r.getDouble("salaire"));
            }

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (cnx != null) {
                try {
                    cnx.close(); // Fermeture de la connexion à la base de donnée
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void Insert(Employe emp) {
        // à partir de JDBC 4, il n'est plus necessaire de charger le driver avec Class.forName

        // On peut utiliser un try with ressource pour fermer automatiquement la connexion à la base de donnée
        try (Connection cnx = DriverManager.getConnection("jdbc:mysql://localhost:3306/formation_java", "root", "")) {
            String req = "INSERT INTO employes(prenom,nom,date_naissance,email,salaire) VALUES(?,?,?,?,?)";
            // PreparedStatement => permet d'exécuter aussi une requête SQL
            // à privilégier, si la requête contient des paramètres: permet d'éviter les
            // concaténations, les types sont vérifiés ...
            PreparedStatement ps = cnx.prepareStatement(req, Statement.RETURN_GENERATED_KEYS);
            // Statement.RETURN_GENERATED_KEYS permet de récupérer la clé primaire généré
            // par la bdd

            // Les méthodes setString, setDouble ... permettent de donnée des valeurs aux
            // paramètres de la requête
            // L'indice commence à 1
            ps.setString(1, emp.getPrenom());
            ps.setString(2, emp.getNom());
            ps.setDate(3, Date.valueOf(emp.getDateNaissance())); // Date.valueOf pour convertir LocalDate en java.sql.Date
            ps.setString(4, emp.getEmail());
            ps.setDouble(5, emp.getSalaire());
            ps.executeUpdate();

            // Récupérer la valeur de la clé primaire générée par la base de donnée
            ResultSet k = ps.getGeneratedKeys();
            if (k.next()) {
                emp.setId(k.getLong(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Employe Select(long id) {
        Employe emp = null;
        try (Connection cnx = DriverManager.getConnection("jdbc:mysql://localhost:3306/formation_java", "root", "")) {
            String req = "SELECT prenom,nom,date_naissance,email,salaire FROM employes WHERE id=?";
            PreparedStatement ps = cnx.prepareStatement(req);
            ps.setLong(1, id);
            ResultSet r = ps.executeQuery();
            if (r.next()) {
                emp = new Employe(r.getString("prenom"), r.getString("nom"), r.getDate("date_naissance").toLocalDate(),
                        r.getString("email"), r.getDouble("salaire"));  // toLocalDate pour convertir  java.sql.Date en LocalDate
                emp.setId(id);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return emp;
    }

    public static void delete(Employe e) {
        try (Connection cnx = DriverManager.getConnection("jdbc:mysql://localhost:3306/formation_java", "root", "")) {
            String req = "DELETE FROM employes WHERE id=?";
            PreparedStatement ps = cnx.prepareStatement(req);
            ps.setLong(1, e.getId());
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }

    // si error est à true, on génére une SqlException entre les 2 requêtes
    // Aucune des 2 n'est executé => rollback, annule la première requête
    public static void testTransaction(boolean error) {
        Connection cnx = null;
        try {
            cnx = DriverManager.getConnection("jdbc:mysql://localhost:3306/formation_java", "root", "");
            cnx.setAutoCommit(false); // Désactivation du mode auto-commit

            Statement stm = cnx.createStatement();

            String req = "INSERT INTO employes(prenom,nom,date_naissance,email,salaire) "
                    + "VALUES ('Alan','Smithee','1960/03/09','asmith@dawan.fr',1800.0)";
            stm.executeUpdate(req);
            if (error) {
                throw new SQLException();
            }
            req = "INSERT INTO employes(prenom,nom,date_naissance,email,salaire) "
                    + "VALUES ('Jo','Dalton','1997/03/09','jo@dawan.fr',1600.0)";
            stm.executeUpdate(req);

            cnx.commit(); // commit() => permet de valider les requêtes
        } catch (SQLException e) {
            try {
                cnx.rollback(); // rollback() => pour annuler les requêtes
                System.err.println("Rollback");
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            if (cnx != null) {
                try {
                    cnx.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
