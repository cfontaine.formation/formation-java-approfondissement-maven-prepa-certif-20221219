package fr.dawan.formation;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import java.awt.GridLayout;

public class Calculatrice {

    private final String[] btnName = { "CE", "C", "<=", "/", "7", "8", "9", "x", "4", "5", "6", "-", "1", "2", "3", "+",
            "+/-", "0", ",", "=" };

    private JFrame frame;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Calculatrice window = new Calculatrice();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public Calculatrice() {
        initialize();
    }

    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 491, 443);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JLabel lblResultat = new JLabel("0");
        lblResultat.setPreferredSize(new Dimension(400,70));
        lblResultat.setFont(new Font("Tahoma", Font.PLAIN, 30));
        lblResultat.setHorizontalAlignment(SwingConstants.RIGHT);
        frame.getContentPane().add(lblResultat, BorderLayout.NORTH);
        
        JPanel panel = new JPanel();
        frame.getContentPane().add(panel, BorderLayout.CENTER);
        panel.setLayout(new GridLayout(5, 4, 2, 2));
        
        CalculatriceAction ca = new CalculatriceAction(lblResultat);
        
        for (int i = 0; i < 20; i++) {
            JButton btn = new JButton(btnName[i]);
            btn.setFont(new Font("Tahoma", Font.PLAIN, 20));
            btn.setActionCommand(btnName[i]);
            btn.addActionListener(ca);
            panel.add(btn);
        }

    }

}
