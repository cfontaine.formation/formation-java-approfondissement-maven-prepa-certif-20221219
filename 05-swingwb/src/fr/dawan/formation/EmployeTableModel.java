package fr.dawan.formation;

import java.time.LocalDate;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import fr.dawan.formation.beans.Employe;

public class EmployeTableModel extends AbstractTableModel {

    private static final long serialVersionUID = 1L;

    private List<Employe> employe;

    public void setEmploye(List<Employe> employe) {
        this.employe = employe;
    }

    public EmployeTableModel(List<Employe> employe) {
        super();
        this.employe = employe;
    }

    @Override
    public int getRowCount() {
        return employe.size();
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Employe row = employe.get(rowIndex);
        switch (columnIndex) {
        case 0:
            return row.getId();
        case 1:
            return row.getPrenom();
        case 2:
            return row.getNom();
        case 3:
            return row.getDateNaissance();
        case 4:
            return row.getEmail();
        case 5:
            return row.getSalaire();
        }
        return null;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
        case 0:
            return "Id";
        case 1:
            return "Prenom";
        case 2:
            return "Nom";
        case 3:
            return "DateNaissance";
        case 4:
            return "Email";
        case 5:
            return "Salaire";
        }
        return "";
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
        case 0:
            return Long.class;
        case 3:
            return LocalDate.class;
        case 5:
            return Double.class;
            default:
                return String.class;
        }

    }

}
