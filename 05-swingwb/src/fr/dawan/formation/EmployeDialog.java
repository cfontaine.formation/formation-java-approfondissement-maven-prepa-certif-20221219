package fr.dawan.formation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import fr.dawan.formation.beans.Employe;

public class EmployeDialog extends JDialog implements ActionListener {

    private static final long serialVersionUID = 1L;



    private Employe employe;
    private JTextField textFieldPrenom=new JTextField(50);
    private JTextField textFieldNom=new JTextField(50);
    private JTextField textFieldDateNaissance=new JTextField(10);
    private JTextField textFieldEmail=new JTextField(150);
    private JTextField textFieldSalaire=new JTextField(10);

    private JLabel lblErrPrenom = new JLabel("");
    private JLabel lblErrNom = new JLabel("");
    private JLabel lblErrDateNaissance = new JLabel("");
    private JLabel lblErrEmail = new JLabel("");
    private JLabel lblErrSalaire = new JLabel("");

    /**
     * @wbp.parser.constructor
     */
    public EmployeDialog(String titre) {
        this(titre, null);
    }

    public EmployeDialog(String titre, Employe employe) {

        setTitle(titre);
        setResizable(false);
        BorderLayout borderLayout = new BorderLayout();
        getContentPane().setLayout(borderLayout);

        JPanel contentPanel = initContentPanel();

        if (employe == null) {
            this.employe = new Employe();
        } else {
            this.employe = employe;
            initTextField(employe);
        }
        
        JPanel pan1 = new JPanel();
        FlowLayout flowLayout = (FlowLayout) pan1.getLayout();
        flowLayout.setVgap(10);
        flowLayout.setHgap(10);
        pan1.add(contentPanel);
        getContentPane().add(pan1, BorderLayout.CENTER);

        JPanel buttonPane = new JPanel();
        FlowLayout fl_buttonPane = new FlowLayout(FlowLayout.CENTER);
        fl_buttonPane.setHgap(20);
        buttonPane.setLayout(fl_buttonPane);
        getContentPane().add(buttonPane, BorderLayout.SOUTH);

        JButton okButton = new JButton("Confirmer");
        okButton.setActionCommand("OK");
        okButton.addActionListener(this);
        buttonPane.add(okButton);
        getRootPane().setDefaultButton(okButton);

        JButton cancelButton = new JButton("Annuler");
        cancelButton.setActionCommand("Cancel");
        cancelButton.addActionListener(this);
        buttonPane.add(cancelButton);
        this.pack();
    }

    public Employe show(Component parent) {
        setModal(true);
        setLocationRelativeTo(parent);
        setVisible(true); // Bloquer jusqu'à un setvisible false
        return employe;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("OK")) {
            clearErrLabel();
            if(!checkInputNotEmpty()) {
                this.pack();
                return;   
            }
            if(!checkInputSize()) {
                this.pack();
                return; 
            }
            LocalDate dateNaissance=convertToLocalDate(textFieldDateNaissance.getText(), lblErrDateNaissance);
            if(dateNaissance==null) {
                this.pack();
                return ;
            }
            Double salaire=convertToDouble(textFieldSalaire.getText() , lblErrSalaire);   
            if(salaire==null) {
                this.pack();
                return;
        }
            employe.setPrenom(textFieldPrenom.getText());
            employe.setNom(textFieldNom.getText());
            employe.setDateNaissance(dateNaissance);
            employe.setEmail(textFieldEmail.getText());
            employe.setSalaire(salaire);
        } else {
            employe = null;
        }
        setVisible(false);
    }

    private JPanel initContentPanel() {
        JPanel contentPanel = new JPanel();
        contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.Y_AXIS));

        JLabel lblPrenom = new JLabel("Prénom");
        lblPrenom.setFont(new Font("Tahoma", Font.PLAIN, 14));
        contentPanel.add(lblPrenom);
        contentPanel.add(textFieldPrenom);
        textFieldPrenom.setColumns(10);
        lblErrPrenom.setForeground(new Color(255, 0, 0));
        contentPanel.add(lblErrPrenom);

        JLabel lblNom = new JLabel("Nom");
        lblNom.setFont(new Font("Tahoma", Font.PLAIN, 14));
        contentPanel.add(lblNom);
        contentPanel.add(textFieldNom);
        textFieldNom.setColumns(10);
        lblErrNom.setForeground(new Color(255, 0, 0));
        contentPanel.add(lblErrNom);

        JLabel lblDateNaissance = new JLabel("Date de naissance (yyyy-mm-dd)");
        lblDateNaissance.setFont(new Font("Tahoma", Font.PLAIN, 14));
        lblDateNaissance.setHorizontalAlignment(SwingConstants.LEFT);
        contentPanel.add(lblDateNaissance);
        contentPanel.add(textFieldDateNaissance);
        textFieldDateNaissance.setColumns(10);

        lblErrDateNaissance.setForeground(new Color(255, 0, 0));
        contentPanel.add(lblErrDateNaissance);

        JLabel lblEmail = new JLabel("Email");
        lblEmail.setFont(new Font("Tahoma", Font.PLAIN, 14));
        contentPanel.add(lblEmail);
        contentPanel.add(textFieldEmail);
        textFieldEmail.setColumns(10);

        lblErrEmail.setForeground(new Color(255, 0, 0));
        contentPanel.add(lblErrEmail);

        JLabel lblSalaire = new JLabel("Salaire");
        lblSalaire.setFont(new Font("Tahoma", Font.PLAIN, 14));
        contentPanel.add(lblSalaire);
        contentPanel.add(textFieldSalaire);
        textFieldSalaire.setColumns(10);

        lblErrSalaire.setForeground(new Color(255, 0, 0));
        contentPanel.add(lblErrSalaire);
        return contentPanel;
    }

    private void initTextField(Employe emp) {
        textFieldPrenom.setText(emp.getPrenom());
        textFieldNom.setText(emp.getNom());
        textFieldDateNaissance.setText(emp.getDateNaissance().toString());
        textFieldEmail.setText(emp.getEmail());
        textFieldSalaire.setText(Double.toString(emp.getSalaire()));
    }

    private boolean checkInputNotEmpty() {
        boolean isOk = checkTextFieldIsEmpty(textFieldPrenom, lblErrPrenom, "Le prénom est vide");
        isOk &= checkTextFieldIsEmpty(textFieldNom, lblErrNom, "Le nom est vide");
        isOk &= checkTextFieldIsEmpty(textFieldDateNaissance, lblErrDateNaissance, "La date de naissance est vide");
        isOk &= checkTextFieldIsEmpty(textFieldEmail, lblErrEmail, "L'email est vide");
        isOk &= checkTextFieldIsEmpty(textFieldSalaire, lblErrEmail, "Le salaire est vide");
        return isOk;
    }
    
    private boolean checkInputSize() {
        boolean isOk = checkTextFieldSize(textFieldPrenom,50, lblErrPrenom, "Le prénom doit être < à 50 caractères");
        isOk &= checkTextFieldSize(textFieldNom, 50,lblErrNom, "Le nom doit être < à 50 caractères");
        isOk &= checkTextFieldSize(textFieldDateNaissance,10, lblErrDateNaissance, "La date de naissance doit être < à 10 caractères");
        isOk &= checkTextFieldSize(textFieldEmail,255, lblErrEmail, "L'email doit être < à 255 caractères");
        isOk &= checkTextFieldSize(textFieldSalaire,10, lblErrEmail, "Le salaire doit être < à 10 caractères");
        return isOk;
    }

    private LocalDate convertToLocalDate(String input, JLabel err) {
        LocalDate tmp = null;
        try {
            tmp = LocalDate.parse(input);
            err.setText("");
        } catch (Exception e) {
            err.setText("Erreur de format: " + e.getMessage());
        }
        return tmp;
    }
    
    private Double convertToDouble(String input, JLabel err) {
        Double tmp = null;
        try {
            tmp = Double.valueOf(input);
            err.setText("");
        } catch (Exception e) {
            err.setText("Erreur de format: " + e.getMessage());
        }
        return tmp;
    }

    private boolean checkTextFieldIsEmpty(JTextField tf, JLabel err, String message) {
        if (tf.getText().isEmpty()) {
            err.setText(message);
            return false;
        }
        return true;
    }
    
    private boolean checkTextFieldSize(JTextField tf,int size, JLabel err, String message) {
        if (tf.getText().length()>size) {
            err.setText(message);
            return false;
        }
        return true;
    }
    
    private void clearErrLabel() {
        lblErrPrenom.setText("");
        lblErrNom.setText("");
        lblErrDateNaissance.setText("");
        lblErrEmail.setText("");
        lblErrSalaire.setText("");
    }
}
