package fr.dawan.formation;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import fr.dawan.formation.beans.Employe;
import fr.dawan.formation.dao.EmployeDao;

public class Application {

    private JFrame frame;

    private JTable table;
    private EmployeTableModel model;

    private JButton btnSupprimer;
    private JButton btnModifier;

    private EmployeDao dao;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Application window = new Application();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public Application() {
        dao = new EmployeDao();
        initialize();

    }

    private void initialize() {
        frame = new JFrame();
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                closeApplication();
            }
        });
        frame.setTitle("Gestion Employés");
        frame.setBounds(100, 100, 812, 513);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel ButonPan = new JPanel();
        FlowLayout flowLayout = (FlowLayout) ButonPan.getLayout();
        flowLayout.setHgap(15);
        frame.getContentPane().add(ButonPan, BorderLayout.SOUTH);

        JButton btnAjouter = initButtonAjouter();
        ButonPan.add(btnAjouter);

        initButtonModifier();
        ButonPan.add(btnModifier);

        initButtonSupprimer();
        ButonPan.add(btnSupprimer);

        JButton btnQuitter = initButtonQuitter();
        ButonPan.add(btnQuitter);

        initializeTable();
    }

    private void closeApplication() {
        int choix = JOptionPane.showConfirmDialog(frame, "Voulez quitter l'application?", "Gestion employés",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (choix == JOptionPane.YES_OPTION) {
            frame.setVisible(false);
            frame.dispose();
            System.exit(0);
        }
    }

    private JButton initButtonQuitter() {
        JButton btn = new JButton("Quitter");
        btn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                closeApplication();
            }
        });
        return btn;
    }

    private JButton initButtonAjouter() {
        JButton btn = new JButton("Ajouter");
        btn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                EmployeDialog d = new EmployeDialog("Ajouter un employé");
                Employe emp = d.show(frame);
                d.dispose();
                if (emp != null) {
                    try {
                        dao.save(emp, false);
                        model.setEmploye(dao.findAll(true));
                        model.fireTableDataChanged();
                    } catch (Exception e1) {
                        JOptionPane.showMessageDialog(null, e1.getMessage(), "Ajouter un employé",
                                JOptionPane.ERROR_MESSAGE);
                        e1.printStackTrace();
                    }
                }
            }
        });
        return btn;
    }

    private void initButtonModifier() {
        btnModifier = new JButton("Modifier");
        btnModifier.setEnabled(false);
        btnModifier.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    Long id = (Long) table.getModel().getValueAt(table.getSelectedRow(), 0);
                    Employe emp = dao.findById(id, true);
                    EmployeDialog d = new EmployeDialog("Modifier un employé", emp);
                    emp = d.show(frame);
                    d.dispose();
                    if (emp != null) {
                        dao.update(emp, false);
                        model.setEmploye(dao.findAll(true));
                        model.fireTableDataChanged();
                    }
                } catch (Exception e1) {
                    JOptionPane.showMessageDialog(null, e1.getMessage(), "Modifer un employé",
                            JOptionPane.ERROR_MESSAGE);
                    e1.printStackTrace();
                }
            }
        });
    }

    private void initButtonSupprimer() {
        btnSupprimer = new JButton("Supprimer");
        btnSupprimer.setEnabled(false);
        btnSupprimer.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {

                    List<Employe> lst = dao.findAll(false);
                    Employe empDelete = lst.remove(table.getSelectedRow());
                    int choix = JOptionPane.showConfirmDialog(frame,
                            "Voulez supprimer " + empDelete.getPrenom() + " " + empDelete.getNom() + " (id="
                                    + empDelete.getId() + ") ?",
                            "Quitter", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                    if (choix == JOptionPane.YES_OPTION) {
                        dao.remove(empDelete, true);
                        model.setEmploye(lst);
                        model.fireTableDataChanged();
                    }
                } catch (Exception e1) {
                    JOptionPane.showMessageDialog(null, e1.getMessage(), "Supprimer un employé",
                            JOptionPane.ERROR_MESSAGE);
                    e1.printStackTrace();
                }
            }
        });
    }

    private void initializeTable() {
        JScrollPane scrollPane = new JScrollPane();
        frame.getContentPane().add(scrollPane, BorderLayout.CENTER);
        table = new JTable();
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION); // limiter la sélection à une ligne

        try {
            model = new EmployeTableModel(dao.findAll(true));
            table.setModel(model);
        } catch (Exception e1) {
            JOptionPane.showMessageDialog(null, e1.getMessage(), "Intialisation application",
                    JOptionPane.ERROR_MESSAGE);
            e1.printStackTrace();
        }

        table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
                boolean selected = table.getSelectedRow() >= 0;
                btnSupprimer.setEnabled(selected);
                btnModifier.setEnabled(selected);
            }
        });
        scrollPane.setViewportView(table);
    }
}
