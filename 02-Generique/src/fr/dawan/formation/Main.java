package fr.dawan.formation;

import java.util.ArrayList;
import java.util.List;

public class Main {

    private static final int ArrayList = 0;

    public static void main(String[] args) {
        // Avant Java 5
        List lst = new ArrayList();
        lst.add("azerty");
        String str = (String) lst.get(0);

        // à partir Java 5
        List<String> lstGen = new ArrayList<>();
        lstGen.add("azerty");
        str = lstGen.get(0);

        // Classe Générique
        Box<String, Integer> b1 = new Box<>("azerty", 42);
        System.out.println(b1);
        String s1 = b1.getV1();
        System.out.println(s1);

        Box<Boolean, Double> b2 = new Box<>(true, 12.3);
        System.out.println(b2);

        // boolean tst1=Main.<String>egalite("azerty", "tyu");
        boolean tst1 = egalite("azerty", "tyu");
        System.out.println(tst1);

        boolean tst2 = egalite(42, 42);
        System.out.println(tst2);

        // Contrainte
        // Box<Boolean,String> b3=new Box<>(true,"aaaaa");

        // Instantiation de type générique
        try {
            String res = b1.create(String.class);
            System.out.println(res);
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

        List<String> lstr = new ArrayList<>();
        lstr.add("Bonjour");
        lstr.add("helo");
        lstr.add("Hello-Wworld");

        // List<Object> listb=lstr:
        List<?> listA = lstr;
        List<Integer> lstI = new ArrayList<>();
        // listA.add("azerty");
        // listA.addAll(lstI);
        listA.clear();
        listA.size();
        listA = lstI;
    }

    public static <T> boolean egalite(T a, T b) {

        return a.equals(b);
    }
}
