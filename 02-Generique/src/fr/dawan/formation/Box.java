package fr.dawan.formation;

public class Box<T,N extends Number> {
    
    private T v1;
    
    private N v2;
    
    // private static N v3; pas de variable de classe génerique

    public Box(T v1, N v2) {
        this.v1 = v1;
        this.v2 = v2;
    }

    public T getV1() {
        return v1;
    }

    public void setV1(T v1) {
        this.v1 = v1;
    }

    public N getV2() {
        return v2;
    }

    public void setV2(N v2) {
        this.v2 = v2;
    }
    
//    public T create() { // On en peut instantier un générique
//        return new T();
//    }
    
    public  T create(Class<T> clazz) throws InstantiationException, IllegalAccessException {
        return clazz.newInstance();
    }

    @Override
    public String toString() {
        return "Box [v1=" + v1 + ", v2=" + v2 + "]";
    }
    
}
