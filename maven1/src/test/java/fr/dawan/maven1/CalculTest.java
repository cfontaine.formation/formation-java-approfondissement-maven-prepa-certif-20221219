package fr.dawan.maven1;

import static org.junit.Assert.assertNotEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import junit.framework.TestCase;

public class CalculTest extends TestCase {

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("setUpBeforeClass");
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("tearDownAfterClass");
    }

    @Before
    protected void setUp() {
        System.out.println("setUp");
    }

    @After
    protected void tearDown()  {
        System.out.println("tearDown");
    }
    
    @Test
    public void testSomme() {
        assertEquals(6, Calcul.somme(2, 4));
        assertNotEquals(0.0, Calcul.somme(1, 4));
    }

    @Test
    public void testDivision() {
        assertEquals(3.0, Calcul.division(6.0, 2.0));
    }

    @Test
    public void testDiivisionParZero() {
        try {
            Calcul.division(3.0, 0.0);
            assertTrue(false);
        } catch (ArithmeticException e) {
            assertTrue(true);
        }
    }
}
