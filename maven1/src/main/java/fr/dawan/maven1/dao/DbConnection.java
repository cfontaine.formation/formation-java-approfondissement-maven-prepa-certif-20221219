package fr.dawan.maven1.dao;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class DbConnection {

    private Connection cnx;

    private Properties prop;

    public Connection getConnection() throws Exception {
        if (cnx == null) {
            if (prop == null) {
                // Pour charger le fichier de prooriété dans le dosier src/main/resources
                ClassLoader loader = Thread.currentThread().getContextClassLoader();
                try (InputStream fr = loader.getResourceAsStream("configuration.properties")) {
                    // Les paramètres de connexion de la base de données sont stockés dans le
                    // fichiers de propriété : configuration.properties
                    prop = new Properties();
                    prop.load(fr);
                }
            }
            // Créer la connexion à la base de donnée
            cnx = DriverManager.getConnection(prop.getProperty("url"), prop.getProperty("user"),
                    prop.getProperty("password"));
        }
        return cnx;
    }

    public void closeConnection(boolean close) throws Exception {
        if (close) {
            cnx.close();
            cnx = null;
        }
    }

}
