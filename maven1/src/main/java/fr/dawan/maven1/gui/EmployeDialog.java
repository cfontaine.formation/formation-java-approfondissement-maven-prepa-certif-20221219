package fr.dawan.maven1.gui;


import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import fr.dawan.maven1.beans.Employe;

public class EmployeDialog extends JDialog implements ActionListener {


    private static final long serialVersionUID = 1L;
    
    private final JPanel contentPanel = new JPanel();
    private JTextField textFieldPrenom;
    
    private Employe employe;

    public EmployeDialog() {
        setBounds(100, 100, 450, 300);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(new GridLayout(15, 2, 10, 0));
        JLabel lblNewLabel = new JLabel("Prénom");
        lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
        contentPanel.add(lblNewLabel);

        textFieldPrenom = new JTextField();
        contentPanel.add(textFieldPrenom);
        textFieldPrenom.setColumns(10);

        JPanel buttonPane = new JPanel();
        buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
        getContentPane().add(buttonPane, BorderLayout.SOUTH);
        
        JButton okButton = new JButton("OK");
        okButton.setActionCommand("OK");
        okButton.addActionListener(this);
        buttonPane.add(okButton);
        getRootPane().setDefaultButton(okButton);
        
        JButton cancelButton = new JButton("Cancel");
        cancelButton.setActionCommand("Cancel");
        cancelButton.addActionListener(this);
        buttonPane.add(cancelButton);

    }



    public Employe show(Component parent) {
        setModal(true);
        setLocationRelativeTo(parent);
        setVisible(true); // Bloquer jusqu'à un setvisible false
        return employe;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("OK")) {
            employe = new Employe();
            employe.setPrenom(textFieldPrenom.getText());
            employe.setNom("afaire");
            employe.setDateNaissance(LocalDate.now());
            employe.setEmail("todo@dawan.com");
            employe.setSalaire(2022.0);
        } else {
            employe = null;
        }
        setVisible(false);
    }

}
