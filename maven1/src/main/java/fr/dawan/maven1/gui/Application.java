package fr.dawan.maven1.gui;


import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import fr.dawan.maven1.beans.Employe;
import fr.dawan.maven1.dao.EmployeDao;

public class Application {

    private JFrame frmGestionEmploys;
    private JTable table;
    private EmployeTableModel model;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Application window = new Application();
                    window.frmGestionEmploys.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public Application() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frmGestionEmploys = new JFrame();
        frmGestionEmploys.setTitle("Gestion Employés");
        frmGestionEmploys.setBounds(100, 100, 812, 513);
        frmGestionEmploys.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel BoutonPan = new JPanel();
        frmGestionEmploys.getContentPane().add(BoutonPan, BorderLayout.SOUTH);

        JButton btnQuitter = new JButton("Quitter");
        btnQuitter.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int choix = JOptionPane.showConfirmDialog(frmGestionEmploys, "Voulez quitter l'application?", "Quitter",
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if (choix == JOptionPane.YES_OPTION) {
                    frmGestionEmploys.setVisible(false);
                    frmGestionEmploys.dispose();
                    System.exit(0);
                }
            }
        });

        JButton btnNewButton = new JButton("Ajouter");
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                EmployeDialog d = new EmployeDialog();
                Employe emp = d.show(frmGestionEmploys);
                d.dispose();
                if (emp != null) {
                    EmployeDao dao = new EmployeDao();
                    try {
                        dao.save(emp, true);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                    model.fireTableDataChanged();
                }
            }
        });
        BoutonPan.add(btnNewButton);
        BoutonPan.add(btnQuitter);

        JScrollPane scrollPane = new JScrollPane();
        frmGestionEmploys.getContentPane().add(scrollPane, BorderLayout.CENTER);

        table = new JTable();

        EmployeDao dao = new EmployeDao();

        try {
            model = new EmployeTableModel(dao.findAll(true));
            table.setModel(model);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        scrollPane.setViewportView(table);
    }

}
