package fr.dawan.formation;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Application {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Exemple Swing");
        // Obtenir la taille de l'écran
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        System.out.println(screenSize);

        frame.setSize(800, 600);
        // frame.setLocation(50,50);
        // frame.setMinimumSize(new Dimension(320, 200));

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JButton bp1 = new JButton("Ok");
        JButton bp2 = new JButton("not Ok");
        bp1.setPreferredSize(new Dimension(150, 50));
        bp2.setPreferredSize(new Dimension(150, 50));

        // Layout Manager -> positionnement des composants

        // Positionnement absolue
//        frame.setLayout(null);
//        bp1.setBounds(500, 500, 100,30);
//        bp2.setBounds(30, 250, 200, 100);

        // FlowLayout
        // frame.setLayout(new FlowLayout(FlowLayout.LEFT,100,50));

        // BorderLayout
//        frame.setLayout(new BorderLayout(20,20));
//        frame.getContentPane().add(bp1,BorderLayout.CENTER);
//        frame.getContentPane().add(bp2,BorderLayout.PAGE_END);

        // GridLayout
//        frame.setLayout(new GridLayout(5,4,30,10));
//        for(int i=0;i<20;i++) {
//            frame.getContentPane().add(new JButton("Bouton "+ i));
//        }

        JPanel pan1 = new JPanel();
        pan1.add(bp1);
        pan1.add(bp2);

        frame.getContentPane().add(pan1, BorderLayout.PAGE_END);

        // Action Listenner
        // 1 - Classe annonyme
        bp1.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                int choix = JOptionPane.showConfirmDialog(frame, "Afficher une autre boite de dialogue", "question",JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE);

                if (choix == JOptionPane.YES_OPTION) {
                    JOptionPane.showMessageDialog(frame, "Message de la boite de dialogue",
                            "Titre de la boite de ddialogue", JOptionPane.INFORMATION_MESSAGE);
                }
                String reponse=JOptionPane.showInputDialog(frame, "Entrer une chaine de caratère","init");
                System.out.println(reponse);
            }
        });

        // 2 - implémenter L'interface ActionListener
        BoutonAction ba = new BoutonAction();
        bp1.addActionListener(ba);
        bp2.addActionListener(ba);
        
        bp1.setActionCommand("bouton 1");
        bp2.setActionCommand("bouton 2");

        bp1.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseEntered(MouseEvent e) {
                System.out.println("Entrer " + e.getX());
            }

        });

        bp1.addKeyListener(new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent e) {
                System.out.println(e.getKeyCode() + " " + e.getKeyChar() + " " + e.getModifiers());
            }

        });

        // frame.pack();
        frame.setVisible(true);

    }

}
